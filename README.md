# FactoryUmbra

**TODO: Add description**


### Hypertables


####  Time Series Provider


######  Storage Agent

- docker://timescale/timescaledb:latest-pg11:5432/tcp->0.0.0.0:55432


######  Sensor Timeseries Sink

- docker://factory_umbra:latest
  - Bus.source Factory.Timeseries.Store ?= "sensor-tsr-bus"
    - |>  Timescale.sink Factory.Timeseries.Store.Hypertable
    - |>  Bus.sink Factory.Timeseries ?= "sensor-tsr"


######  Sensor Timeseries Source


- docker://factory_umbra:latest
  - Bus.source Factory.Timeseries
  -   |>  assert { :sensorId, :ts, :timeSpan, :timeStep }


- docker://factory_umbra:latest
  - Factory.Timeseries.pull { :sensorId, :ts, :timeSpan, :timeStep }
    - |>  Bus.sink Timeseries.Store, Factory.Timeseries
