defmodule Timeseries.Store do
  @moduledoc """
  Documentation for Timeseries.Store.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Timeseries.Store.hello()
      :world

  """
  def hello do
    :world
  end
end
